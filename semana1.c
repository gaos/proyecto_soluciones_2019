#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*Te ahorrare un poco de tiempo
 * Por ahora compila, pero la generación no se cumple
 * la verificación funciona para la segunda generación solo en la 
 * primera letra de la matriz, y esta es retornada a la matriz, pero el
 * error es que retorna solamente a la primera letra de la generación
 * anterior*/
int columnas_y_filas(int i, int j, int tamano, int k, char matriz[tamano][tamano][k], char letra){
 
	int l=i;
	for(k=0;k<tamano;k++){
		for(l=0;l<tamano;l++){
			switch(letra){
				case 'A':
						//	4 filas anteriores misma columna
							for(l=i-4;l<i;l++){
								if(l>=0){
									if(matriz[l][j][k]=='A'){
										return 'A';
										}
									}
								}
									break;
				case 'T': 
							//4 columnas en la misma fila
							for(l=j+4;l>j;l--){
								if(l>=tamano){
									if(matriz[i][l][k] == 'T'){
										return'T';
										}
									}
								}
									break;
				case 'C': 
							//4 columnas anteriores de la misma fila 
							for(l=j-4;l<j;l++){
								if(l>=0){
									if(matriz[i][l][k] == 'C'){
										return'C';
										}
									}
								}
									break;
				case 'G': 
				/*4 filas siguientes */
							for(l=i+4;l>i;l--){
								if(l<=tamano){
									if(matriz[l][j][k]=='G'){
										return 'G';
										}
									}
								}
									break;
				}
			}
		}
	return 'x';
}

int contadores(int tamano, int i, int j,int k, char matriz[tamano][tamano][k], char letra1, char letra2){

/*En esta función se cuenta las veces que se repite
 * correspondiente a la letra que pidan buscar*/
 
	int contador=0,l,m;

	for(m=i-1;m<i+1;m++){
		for(l=j-1;l<j+1;l++){
			switch(letra1){
				
				case 'A': if(matriz[m][l][k]==letra2){
							contador=contador+1;
							}
							break;
				case 'T': if(matriz[m][l][k]==letra2){
							contador=contador+1;
							}
							break;
				case 'C': if(matriz[m][l][k]==letra2){
							contador=contador+1;
							}
							break;
				case 'G': if(matriz[m][l][k]==letra2){
							contador=contador+1;
							}
							break;	
				}
			}
		}
	return contador;
}

int verificador( int tamano,int k, char matriz[tamano][tamano][k]){
int i,j, contar, random;
char temporal;
/*Se observa la primera condicion*/
for(i=0;i<tamano;i++){
	for(j=0;j<tamano;j++){
		switch(matriz[i][j][k]){
			case 'A': contar=contadores(tamano, i, j, k, matriz, matriz[i][j][k], 'G');
							if(contar==3){
							temporal= 'T';
							}
							else{
								temporal= 'A';
								}
								break;
			case 'T': contar=contadores(tamano,i,j, k, matriz,matriz[i][j][k], 'C');
							if(contar==3){
							temporal= 'A';
							}else{
								temporal= 'T';
								}
								break;
			case 'C': contar=contadores(tamano,i,j, k, matriz, matriz[i][j][k], 'T');
							if(contar==3){
							temporal= 'G';
							}
								else{
								temporal= 'C';
								}
								break;
			case 'G': contar=contadores(tamano,i,j, k, matriz, matriz[i][j][k], 'A');
							if(contar==3){
							temporal= 'C';
							}
								else{
								temporal= 'G';
								}
								
			}
		}
	}
if(temporal==matriz[i][j][k]){
	
	/*Si la anterior no se cumple, se observa las columnas y filas
	 * dependiente de las letras*/
	 
	temporal=columnas_y_filas(i,j,tamano,k, matriz, temporal);
	if(matriz[i][j][k]==temporal){
			return temporal;
		}
		else{
			/* El random que es la condicion 9 */
			random=(rand()%4)+1;
			switch(random){
					case 1: matriz[i][j][k]='A'; break;
					case 2: matriz[i][j][k]='T';break;
					case 3: matriz[i][j][k]='C'; break;
					case 4: matriz[i][j][k]='G';break;
					default: printf("\nHa ocurrido un error");
				
				}
			
			}
	}
		return matriz[i][j][k];
}

void wolf_in_sheeps_clothing(){
	
	/*Hay varibles que serán ocupadas en otras partes del proyecto, 
	 * por eso hay de más*/
	 
	int  i, j, nucleotido, cantidad, k=0, l=0, m=0, aumento=0;
	char opcion;

	printf("\nIngrese la cantidad de generaciones (presione dos para ver random y padre): ");
	scanf("%d", &cantidad);

	int tamano[cantidad];

	printf("\nIngrese el tamaño de su matriz: ");
	scanf("%d", &l); 
	while(k<cantidad){
		k++;
		tamano[k]=l;
		char matriz[l][l][cantidad], matriz_tempo[l][l][cantidad];
		printf("\n");
		
		if(k==1){
			for (i=0;i<l;i++){
				for(j=0;j<l;j++){
					nucleotido=(rand()%4)+1;
							switch(nucleotido){
								case 1: matriz[i][j][k]='A'; break;
								case 2: matriz[i][j][k]='T';break;
								case 3: matriz[i][j][k]='C'; break;
								case 4: matriz[i][j][k]='G';break;
								default: printf("\nHa ocurrido un error");
							}	
						printf("[%c]", matriz[i][j][k]);
					} 
					printf("\n");
				}
		
			printf("\nGeneración %d\n", k);
			printf("\n¿Desea acabar el programa? presione s para si \n ");
			scanf(" %c", &opcion);
					if (opcion == 'S' || opcion == 's'){
						break;
					
				}else{
					printf("¿Desea añadir generaciones? presione s para que se realice un aumento\n");
					scanf(" %c", &opcion);
						if(opcion == 'S' || opcion == 's'){
						printf("\nIngrese la cantidad de aumento:");
						scanf("%d", &aumento);
						cantidad=cantidad+aumento;
					}
				}
			
			
			/*El tamaño se iguala a la cantidad, todo debido a que 
			 * no me gusta iniciar con la generación cero
			 * pero a veces debo de admitir que suena bien
			 * como la generación cero de ciertos residentes demoniacos*/
				printf("\nIngrese la generación que quiere volver a observar:\n");
				scanf("%d",&m);
				if((m>cantidad || m<=0) || (m>k)){
					printf("\nNo se puede observar algo superior a ti\n");
					}
					else{
						for (i=0;i<tamano[m];i++){
							for(j=0;j<tamano[m];j++){
								//printf("\033[1;41m");//Fondo rojo
								printf("\033[1;34m");
								printf("[%c]", matriz[i][j][m]);
								printf("\033[0m");
								} 	
							printf("\n");
							}
						}
				} 
				else{
					for(i=0;i<l;i++){
						for(j=0;j<l;j++){
								matriz_tempo[i][j][k]=matriz[i][j][k-1];
							}
						}
					for(i=0;i<l;i++){
						for(j=0;j<l;j++){
								matriz[i][j][k]=verificador(l, k, matriz_tempo);
							}
						}
						for(i=0;i<l;i++){
							for(j=0;j<l;j++){
								printf("[%c]", matriz[i][j][k]);
								
								}printf("\n");
							
							}
						printf("\nGeneracion de arriba es %d\n",k);
					}
		}		
	}

int main(void){

srand(time(NULL));

/*Se llama la función, le puse el nombre de una canción que me gusta
 * porque puedo*/
	wolf_in_sheeps_clothing();

	return 0;
}
